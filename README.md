# Group OSCORE Profile of ACE

This is the working area for the individual Internet-Draft, "Group OSCORE Profile of the Authentication and Authorization for Constrained Environments Framework".

* [Editor's Copy](https://crimson84.gitlab.io/draft-tiloca-ace-group-oscore-profile)
* [Individual Draft](https://datatracker.ietf.org/doc/html/draft-tiloca-ace-group-oscore-profile)
* [Compare Individual Draft and Editor's copy](https://tools.ietf.org/rfcdiff?url1=https://tools.ietf.org/id/draft-tiloca-ace-group-oscore-profile-05.txt&url2=https://crimson84.gitlab.io/draft-tiloca-ace-group-oscore-profile/draft-tiloca-ace-group-oscore-profile.txt)

## Building the Draft

Formatted text and HTML versions of the draft can be built using `make`.

```sh
$ make
```

This requires that you have the necessary software installed.  See
[the instructions](https://github.com/martinthomson/i-d-template/blob/main/doc/SETUP.md).


## Contributing

See the
[guidelines for contributions](https://github.com/martinthomson/i-d-template/blob/main/CONTRIBUTING.md).
